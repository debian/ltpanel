#include<X11/Xlib.h>
#include "lpanel.h"
#include "drawing.h" 


	#define PALETTE_COUNT 6
unsigned long palette[PALETTE_COUNT];

/* FIXME : rip out this horror */
extern GC fore_gc;
extern int scr_depth;
/* end horror */
void
gui_draw_vline (taskbar * tb, int x,int row)
{
	set_foreground (4,tb->dd);
	draw_line (tb, x, row * tb->rowheight, x,(row + 1) * tb->rowheight);
	set_foreground (3,tb->dd);
	draw_line (tb, x + 1, row * tb->rowheight, x + 1,(row + 1) * tb->rowheight);
}

void
draw_line (taskbar *tb, int x, int y, int a, int b)
{
	XDrawLine (tb->dd, tb->win, fore_gc, x, y, a, b);
}

void
set_foreground (int index, Display * dd)
{
	XSetForeground (dd, fore_gc, palette[index]);
}

void
fill_rect (taskbar *tb, int x, int y, int a, int b)
{
	XFillRectangle (tb->dd, tb->win, fore_gc, x, y, a, b);
}

void
scale_icon (task *tk, Display * dd)
{
	int xx, yy, x, y, w, h, d, bw;
	Pixmap pix, mk = None;
	XGCValues gcv;
	GC mgc;

	XGetGeometry (dd, tk->icon, &pix, &x, &y, &w, &h, &bw, &d);
	pix = XCreatePixmap (dd, tk->win, ICONWIDTH, ICONHEIGHT, scr_depth);

	if (tk->mask != None)
	{
		mk = XCreatePixmap (dd, tk->win, ICONWIDTH, ICONHEIGHT, 1);
		gcv.subwindow_mode = IncludeInferiors;
		gcv.graphics_exposures = False;
		mgc = XCreateGC (dd, mk, GCGraphicsExposures | GCSubwindowMode, &gcv);
	}

	set_foreground (3,dd);

	/* this is my simple & dirty scaling routine */
	for (y = ICONHEIGHT - 1; y >= 0; y--)
	{
		yy = (y * h) / ICONHEIGHT;
		for (x = ICONWIDTH - 1; x >= 0; x--)
		{
			xx = (x * w) / ICONWIDTH;
			if (d != scr_depth)
				XCopyPlane (dd, tk->icon, pix, fore_gc, xx, yy, 1, 1, x, y, 1);
			else
				XCopyArea (dd, tk->icon, pix, fore_gc, xx, yy, 1, 1, x, y);
			if (mk != None)
				XCopyArea (dd, tk->mask, mk, mgc, xx, yy, 1, 1, x, y);
		}
	}

	if (mk != None)
	{
		XFreeGC (dd, mgc);
		tk->mask = mk;
	}

	tk->icon = pix;
}
